"""
I will play your python code

michal@michalklich.com

"""
from ConfigParser import ConfigParser
from tempfile import NamedTemporaryFile

from pyknon.genmidi import Midi
from pyknon.music import NoteSeq
import pygame

# Probably there are libs for playing music
# and there are lists of notes
NOTES_SCALE = ('C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B')
CONFIG = {}


def play_file(path):
    pygame.init()
    pygame.mixer.music.load(path.name)
    pygame.mixer.music.play()
    # omg, is there a better way?
    while pygame.mixer.music.get_busy():
        pygame.time.wait(1000)


# def _analyze_line(line, notes, prev_value):
#     last_note = notes[-1:] and notes[-1:][0] or 'C'
#     try:

#         if len(line) > prev_len:  # this might be overwritten by plugins
#             next_note = NOTES_SCALE[NOTES_SCALE.index(last_note) + 1]
#         else:
#             next_note = NOTES_SCALE[NOTES_SCALE.index(last_note) - 1]
#     except IndexError:
#         next_note = last_note

#     return next_note, value

def actual_line_parser(notes, line):
    """
    TODO: better name
    This needs to return a note.
    How is it decided which one is up to person implementing it

    """
    return

def _analyze_line(notes):
    """ TODO: better name """
    def next_note(line):
        """ TODO: better name """
        # process line here
        notes.append(actual_line_parser(notes, line))
        return notes
    return next_note

def _analyze_file(file_):
    """ TODO: better name """
    notes = []
    analyze_line = _analyze_line(notes)
    for line in file_:  # allow different line access?
        analyze_line(line)
    return notes


def _create_midi_seq(notes):
    notes_seq = NoteSeq(' '.join(notes))
    midi = Midi(1, tempo=_get_tempo())
    midi.seq_notes(notes_seq, track=0)
    return midi


def create_midi_file(notes):
    tmp_file = NamedTemporaryFile(suffix='.mid')
    midi = _create_midi_seq(notes)
    midi.write(tmp_file.name)
    return tmp_file


def analyze_file(_file):
    """
    if there is more chars in line then in previous play next note on scale
    if less play previous note

    TODO: maybe play something wrong if pep8 is violated?

    """
    with open(_file) as f:
        notes = _analyze_file(f)

    return create_midi_file(notes)


def parse_config(parser, file_):
    parser.read(file_)
    try:
        CONFIG['TEMPO'] = parser.getint('midi', 'tempo')
    except ValueError:
        CONFIG['TEMPO'] = 150

    try:
        CONFIG['PARSER_MODULE'] = parser.get('parser', 'module')
    except ValueError:
        CONFIG['PARSER_MODULE'] = 'line_length'


def _get_tempo():
    return CONFIG.get('TEMPO')


def _get_parser():
    return CONFIG.get('PARSER_MODULE')


if __name__ == '__main__':
    parse_config(ConfigParser(), 'config.ini')
    play_file(analyze_file('musician.py'))
